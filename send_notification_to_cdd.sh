#!/bin/bash
export CDD_APPLICATION_NAME=$CI_PROJECT_NAME
export CDD_APPLICATION_VERSION_NAME=$CI_COMMIT_BRANCH
export CDD_GIT_COMMIT_ID=$CI_COMMIT_SHA
export CDD_APPLICATION_VERSION_BUILD_NUMBER=$CI_PIPELINE_IID
export CDD_APPLICATION_SOURCE="Gitlab-P"
tempfile=$(mktemp)

echo "CDD_APPLICATION_NAME = " $CI_PROJECT_NAME
echo "CDD_APPLICATION_VERSION_NAME=" $CI_COMMIT_BRANCH
echo "CDD_GIT_COMMIT_ID=" $CI_COMMIT_SHA
echo "CDD_APPLICATION_VERSION_BUILD_NUMBER=" $CI_PIPELINE_IID
echo "CDD_APPLICATION_SOURCE=" $CDD_APPLICATION_SOURCE

code=$(curl -s --header "Content-Type: application/json" --header "Accept: application/json" -d \
"
{
    \"applicationName\": \"$CDD_APPLICATION_NAME\",
    \"applicationSourceName\": \"$CDD_APPLICATION_SOURCE\",
    \"applicationVersionBuildNumber\": \"$CDD_APPLICATION_VERSION_BUILD_NUMBER\",
    \"applicationVersionName\": \"$CDD_APPLICATION_VERSION_NAME\",
    \"commits\": [
        {
            \"commitId\": \"$CDD_GIT_COMMIT_ID\"
        }
    ],
    \"releaseDsl\": {
		\"dslFilename\": \"Online_Banking_Template.json\",
		\"dslParameters\": {
			\"ReleaseVersion\": \"$CDD_APPLICATION_VERSION_NAME\"        
		},
		\"fileSourceName\": \"release-template\",
		\"releaseDslScope\": \"BUSINESS_APPLICATION\"
	}
}	
" \
"$CDD_SERVER_URL/cdd/design/$CDD_TENANT_ID/v1/applications/application-versions/application-version-builds" \
 -H "Authorization: Bearer $CDD_API_KEY"  --write-out '%{http_code}' -o $tempfile)



if [[ $code != 200  ]] ; then
    echo "$CDD_SERVER_URL SAID $code"
    exit 1
fi

jq . $tempfile
echo
importError="$(jq .data.buildNotificationReleaseResponse.importResults[0].error $tempfile)"
rm -f $tempfile

if [ $importError == "null" ]
then
    echo "No errors found in the release DSL import"
    exit 0
    
else 
    echo "Error Message: $importError"
    exit 1 
fi
exit 0
